
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', name: 'index', component: () => import('pages/Index.vue') },
      { path: 'orders', name: 'orders', component: () => import('pages/orders.vue') },
      { path: 'order', name: 'order', component: () => import('pages/Order.vue') },
      { path: 'settings', name: 'settings', component: () => import('pages/settings.vue') },
      { path: 'price', name: 'price', component: () => import('pages/price.vue') },
      { path: 'checkout/:name', name: 'checkout', component: () => import('pages/Checkout.vue'), props: true },
      { path: 'FAQ', name: 'faq', component: () => import('pages/faq.vue') },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')

  })
}

export default routes
